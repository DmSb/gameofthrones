package com.dmisb.gameofthrones.mvp.presenters.impl;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.dmisb.gameofthrones.R;
import com.dmisb.gameofthrones.data.storage.Character;
import com.dmisb.gameofthrones.mvp.models.CharacterModel;
import com.dmisb.gameofthrones.mvp.presenters.ICharacterPresenter;
import com.dmisb.gameofthrones.mvp.views.ICharacterView;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.StringUtils;

public class CharacterPresenter implements ICharacterPresenter {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(CharacterPresenter.class.getName());
    private static final CharacterPresenter sPresenter = new CharacterPresenter();
    private final CharacterModel mModel;
    private ICharacterView mView;
    private Character mFather = null;
    private Character mMother = null;
    private int mHouseId;
    private String mHouseWords;

    private CharacterPresenter() {
        mModel = new CharacterModel();
    }

    public static CharacterPresenter getInstance() {
        return sPresenter;
    }

    @Override
    public void takeView(ICharacterView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView(Intent intent) {
        mHouseId = intent.getIntExtra(ConstantManager.ITEM_HOUSE_KEY, 0);

        Log.d(TAG, "houseId = " + String.valueOf(mHouseId));

        if (mView != null) {

            switch (mHouseId) {
                case ConstantManager.HOUSE_STARK_ID:
                    mView.setImage(R.drawable.stark);
                    break;
                case ConstantManager.HOUSE_LANNISTER_ID:
                    mView.setImage(R.drawable.lannister);
                    break;
                case ConstantManager.HOUSE_TARGARYEN_ID:
                    mView.setImage(R.drawable.targarien);
                    break;
            }

            mView.setName(intent.getStringExtra(ConstantManager.ITEM_NAME_KEY));
            mHouseWords = intent.getStringExtra(ConstantManager.ITEM_WORD_KEY);
            mView.setWords(mHouseWords);
            mView.setBorn(intent.getStringExtra(ConstantManager.ITEM_BORN_KEY));

            String s = intent.getStringExtra(ConstantManager.ITEM_TITLES_KEY);
            if (s != null) {
                s = s.replaceAll(", ", "\n");
                mView.setTitles(s);
            }

            s = intent.getStringExtra(ConstantManager.ITEM_ALIASES_KEY);
            if (s != null) {
                s = s.replaceAll(", ", "\n");
                mView.setAliases(s);
            }

            int id = intent.getIntExtra(ConstantManager.ITEM_FATHER_KEY, 0);
            if (id > 0) {
                mFather = mModel.getCharacter(id);
                if (mFather != null) {
                    mView.setFather(mFather.name, View.VISIBLE);
                } else {
                    mView.setFather(null, View.GONE);
                }
            } else {
                mView.setFather(null, View.GONE);
            }

            id = intent.getIntExtra(ConstantManager.ITEM_MOTHER_KEY, 0);
            if (id > 0) {
                mMother = mModel.getCharacter(id);
                if (mMother != null) {
                    mView.setMother(mMother.name, View.VISIBLE);
                } else {
                    mView.setMother(null, View.GONE);
                }
            } else {
                mView.setMother(null, View.GONE);
            }

            s = intent.getStringExtra(ConstantManager.ITEM_DIED_KEY);
            if (!s.isEmpty()) {
                mView.setDied(s);
            }
        }
    }

    @Override
    public ICharacterView getView() {
        return mView;
    }

    @Override
    public void fillExtra(Intent intent, boolean isFaher) {
        Character character;
        if (isFaher) {
            character = mFather;
        } else {
            character = mMother;
        }

        if (character != null) {
            intent.putExtra(ConstantManager.ITEM_ID_KEY, character.id);
            intent.putExtra(ConstantManager.ITEM_NAME_KEY, character.name);
            intent.putExtra(ConstantManager.ITEM_BORN_KEY, character.born);
            intent.putExtra(ConstantManager.ITEM_TITLES_KEY, character.titles);
            intent.putExtra(ConstantManager.ITEM_DIED_KEY, character.died);
            intent.putExtra(ConstantManager.ITEM_ALIASES_KEY, character.aliases);
            intent.putExtra(ConstantManager.ITEM_FATHER_KEY, character.father);
            intent.putExtra(ConstantManager.ITEM_MOTHER_KEY, character.mother);

            intent.putExtra(ConstantManager.ITEM_WORD_KEY, mHouseWords);
            intent.putExtra(ConstantManager.ITEM_HOUSE_KEY, mHouseId);
        }
    }
}
