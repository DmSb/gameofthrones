package com.dmisb.gameofthrones.mvp.presenters;

import android.content.Intent;

import com.android.annotations.Nullable;
import com.dmisb.gameofthrones.mvp.views.ICharacterView;

public interface ICharacterPresenter {

    void takeView(ICharacterView view);
    void dropView();
    void initView(Intent intent);

    @Nullable
    ICharacterView getView();

    void fillExtra(Intent intent, boolean isFaher);
}
