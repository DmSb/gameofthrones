package com.dmisb.gameofthrones.mvp.views;

public interface ICharacterView {

    void showMessage(String message);

    void setImage(int resId);
    void setName(String name);
    void setWords(String words);
    void setBorn(String born);
    void setTitles(String titles);
    void setAliases(String aliases);
    void setFather(String fatherName, int visibility);
    void setMother(String motherName, int visibility);
    void setDied(String died);
}
