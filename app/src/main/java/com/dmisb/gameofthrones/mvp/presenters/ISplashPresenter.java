package com.dmisb.gameofthrones.mvp.presenters;

import com.android.annotations.Nullable;
import com.dmisb.gameofthrones.mvp.views.ISplashView;

public interface ISplashPresenter {

    void takeView(ISplashView view);
    void dropView();
    void initView();

    @Nullable
    ISplashView getView();
}
