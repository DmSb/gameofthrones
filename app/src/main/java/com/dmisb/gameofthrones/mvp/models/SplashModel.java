package com.dmisb.gameofthrones.mvp.models;

import android.util.Log;

import com.dmisb.gameofthrones.data.managers.DataManager;
import com.dmisb.gameofthrones.data.network.res.CharacterRes;
import com.dmisb.gameofthrones.data.network.res.HouseRes;
import com.dmisb.gameofthrones.data.storage.DataBaseAdapter;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.EvenBus;
import com.dmisb.gameofthrones.utils.StringUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashModel {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(SplashModel.class.getName());
    private DataManager mDataManager;
    private byte mLoadState = 0;

    public SplashModel() {
        mDataManager = DataManager.getInstance();
    }

    public boolean checkLoaded() {
        return mDataManager.getPreferencesManager().getDataLoaded();
    }

    public void setLoaded() {
        Log.d(TAG, "call set all data loaded");
        mDataManager.getPreferencesManager().setDataLoaded();
    }

    public void loadFromServer() {
        DataBaseAdapter.deleteData();
        loadCharacters();
        loadHouse(ConstantManager.HOUSE_TARGARYEN_ID);
        loadHouse(ConstantManager.HOUSE_LANNISTER_ID);
        loadHouse(ConstantManager.HOUSE_STARK_ID);
    }

    private void loadCharacters() {

        for (int i = 1; i <= ConstantManager.PAGE_COUNT; i++) {
            Call<List<CharacterRes>> call = mDataManager.getCharacters(i);
            call.enqueue(new Callback<List<CharacterRes>>() {
                @Override
                public void onResponse(Call<List<CharacterRes>> call, Response<List<CharacterRes>> response) {
                    if (response.code() == 200) {
                        Log.d(TAG, "call save Characters");
                        DataBaseAdapter.saveCharacters(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<CharacterRes>> call, Throwable t) {
                    mDataManager.getBus().post(new EvenBus.EventDb(ConstantManager.NETWORK_ERROR_EVENT));
                }
            });
        }
    }

    private void loadHouse(final int houseId) {

        Call<HouseRes> call = mDataManager.getHouse(houseId);
        call.enqueue(new Callback<HouseRes>() {
            @Override
            public void onResponse(Call<HouseRes> call, Response<HouseRes> response) {
                if (response.code() == 200) {
                    HouseRes house = response.body();
                    house.id = houseId;
                    Log.d(TAG, "call save House");
                    DataBaseAdapter.saveHouse(house);
                }
            }

            @Override
            public void onFailure(Call<HouseRes> call, Throwable t) {
                mDataManager.getBus().post(new EvenBus.EventDb(ConstantManager.NETWORK_ERROR_EVENT));
            }
        });
    }

    public byte getLoadState() {
        return mLoadState;
    }

    public void setLoadState(byte loadState) {
        mLoadState = loadState;
    }
}
