package com.dmisb.gameofthrones.mvp.models;

import android.util.Log;

import com.dmisb.gameofthrones.data.network.res.HouseRes;
import com.dmisb.gameofthrones.data.storage.Character;
import com.dmisb.gameofthrones.data.storage.DataBaseAdapter;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class MainModel {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(MainModel.class.getName());
    private int mHouseId;
    private HouseRes mHouse;
    private List<Character> mCharacters;

    public MainModel() {
        mHouseId = 0;
        mCharacters = new ArrayList<>();
    }

    public void setHouseId(int houseId, String name) {
        if (mHouseId != houseId) {
            Log.d(TAG, "setHouseId = " + String.valueOf(houseId) + ", name = " + name);
            mHouse = DataBaseAdapter.getHouse(houseId);
            mCharacters = DataBaseAdapter.getCharacters(houseId, name);
            mHouseId = houseId;
        }
    }

    public int getHouseId() {
        return mHouseId;
    }

    public HouseRes getHouse() {
        return mHouse;
    }

    public List<Character> getCharacters() {
        return mCharacters;
    }

    public void searchCharacters(String name) {
        mCharacters = DataBaseAdapter.getCharacters(mHouseId, name);
    }
}
