package com.dmisb.gameofthrones.mvp.views;

public interface ISplashView {

    void showProgress();
    void hideProgress();
    void showError(int resId);
    void close();
}
