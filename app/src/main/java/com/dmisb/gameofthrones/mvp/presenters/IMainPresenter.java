package com.dmisb.gameofthrones.mvp.presenters;

import android.content.Intent;
import android.os.Parcelable;

import com.android.annotations.Nullable;
import com.dmisb.gameofthrones.mvp.views.IMainView;

public interface IMainPresenter {

    void takeView(IMainView view);
    void dropView();
    void initView();

    @Nullable
    IMainView getView();

    void onTabSelected(int tabId);
    void onSearch(String query);
    void fillExtra(Intent intent, int position);
    String getQuery();

    void setState(Parcelable state);
    Parcelable getState();
}
