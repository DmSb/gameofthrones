package com.dmisb.gameofthrones.mvp.views;

import com.dmisb.gameofthrones.data.storage.Character;

import java.util.List;

public interface IMainView {

    void showHouse(List<Character> characters, int houseId);
    void setupPager(int tabId);
}
