package com.dmisb.gameofthrones.mvp.presenters.impl;

import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;

import com.dmisb.gameofthrones.data.storage.Character;
import com.dmisb.gameofthrones.mvp.models.MainModel;
import com.dmisb.gameofthrones.mvp.presenters.IMainPresenter;
import com.dmisb.gameofthrones.mvp.views.IMainView;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.StringUtils;

public class MainPresenter implements IMainPresenter {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(MainPresenter.class.getName());
    private static final MainPresenter sPresenter = new MainPresenter();
    private final MainModel mModel;
    private IMainView mView;
    private String mQuery;
    private Parcelable mListState;

    private MainPresenter() {
        mModel = new MainModel();
        mQuery = "";
        mListState = null;
    }

    public static MainPresenter getInstance() {
        return sPresenter;
    }

    @Override
    public void takeView(IMainView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {
        if (mModel.getHouseId() == 0) {
            mModel.setHouseId(ConstantManager.HOUSE_STARK_ID, mQuery);
        }

        Log.d(TAG, "initView");

        if (mView != null) {
            int tabId = 0;
            switch (mModel.getHouseId()) {
                case ConstantManager.HOUSE_LANNISTER_ID:
                    tabId = 1;
                    break;
                case ConstantManager.HOUSE_TARGARYEN_ID:
                    tabId = 2;
                    break;
            }
            mView.setupPager(tabId);
        }

        showHouse();
    }

    @Override
    public IMainView getView() {
        return mView;
    }

    @Override
    public void onTabSelected(int tabId) {
        int oldId = mModel.getHouseId();

        switch (tabId) {
            case 0:
                mModel.setHouseId(ConstantManager.HOUSE_STARK_ID, mQuery);
                break;
            case 1:
                mModel.setHouseId(ConstantManager.HOUSE_LANNISTER_ID, mQuery);
                break;
            case 2:
                mModel.setHouseId(ConstantManager.HOUSE_TARGARYEN_ID, mQuery);
                break;
        }

        showHouse();
    }

    @Override
    public void onSearch(String query) {
        mQuery = query;
        mModel.searchCharacters(query);
        showHouse();
    }

    @Override
    public void fillExtra(Intent intent, int position) {

        Character character = mModel.getCharacters().get(position);

        intent.putExtra(ConstantManager.ITEM_ID_KEY, character.id);
        intent.putExtra(ConstantManager.ITEM_NAME_KEY, character.name);
        intent.putExtra(ConstantManager.ITEM_BORN_KEY, character.born);
        intent.putExtra(ConstantManager.ITEM_TITLES_KEY, character.titles);
        intent.putExtra(ConstantManager.ITEM_DIED_KEY, character.died);
        intent.putExtra(ConstantManager.ITEM_ALIASES_KEY, character.aliases);
        intent.putExtra(ConstantManager.ITEM_FATHER_KEY, character.father);
        intent.putExtra(ConstantManager.ITEM_MOTHER_KEY, character.mother);

        intent.putExtra(ConstantManager.ITEM_WORD_KEY, mModel.getHouse().words);
        intent.putExtra(ConstantManager.ITEM_HOUSE_KEY, mModel.getHouse().id);
    }

    @Override
    public String getQuery() {
        return mQuery;
    }

    @Override
    public void setState(Parcelable state) {
        mListState = state;
    }

    @Override
    public Parcelable getState() {
        return mListState;
    }

    private void showHouse() {
        if (mView != null) {
            Log.d(TAG, "showHouse");
            mView.showHouse(mModel.getCharacters(), mModel.getHouseId());
        }
    }
}
