package com.dmisb.gameofthrones.mvp.models;

import com.dmisb.gameofthrones.data.storage.Character;
import com.dmisb.gameofthrones.data.storage.DataBaseAdapter;

public class CharacterModel {

    public Character getCharacter(int id) {
        return DataBaseAdapter.getCharacter(id);
    }
}
