package com.dmisb.gameofthrones.mvp.presenters.impl;

import android.util.Log;

import com.dmisb.gameofthrones.R;
import com.dmisb.gameofthrones.data.managers.DataManager;
import com.dmisb.gameofthrones.mvp.models.SplashModel;
import com.dmisb.gameofthrones.mvp.presenters.ISplashPresenter;
import com.dmisb.gameofthrones.mvp.views.ISplashView;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.EvenBus;
import com.dmisb.gameofthrones.utils.NetworkStatusChecker;
import com.dmisb.gameofthrones.utils.StringUtils;
import com.squareup.otto.Subscribe;

public class SplashPresenter implements ISplashPresenter {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(SplashPresenter.class.getName());
    private static final SplashPresenter sPresenter = new SplashPresenter();
    private final SplashModel mModel;
    private ISplashView mView;

    private int mHouseCount = 0;
    private int mCharacterPageCount = 0;

    private SplashPresenter() {
        mModel = new SplashModel();
        DataManager.getInstance().getBus().register(this);
    }

    public static SplashPresenter getInstance() {
        return sPresenter;
    }

    @Override
    public void takeView(ISplashView view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void initView() {
        if (mView != null) {
            if (!mModel.checkLoaded()) {
                if (NetworkStatusChecker.isNetworkAvailable()) {
                    switch (mModel.getLoadState()) {
                        case 0:
                            mModel.setLoadState((byte) 1);
                            Log.d(TAG, "call SplashModel.loadFromServer()");
                            mView.showProgress();
                            mModel.loadFromServer();
                            break;
                        case 1:
                            Log.d(TAG, "loadFromServer() already started");
                            mView.showProgress();
                            break;
                        case 2:
                            Log.d(TAG, "loadFromServer() finished");
                            mView.close();
                            break;
                    }
                } else {
                    mView.showError(R.string.network_unavailable_message);
                }
            } else {
                mView.close();
            }
        }
    }

    @Override
    public ISplashView getView() {
        return mView;
    }

    @Subscribe
    public void onEventBus(EvenBus.EventDb event) {
        String msg = event.getMessage();
        switch (msg) {
            case ConstantManager.SAVE_HOUSE_EVENT:
                mHouseCount++;
                Log.d(TAG, "inc houseCount => " + String.valueOf(mHouseCount));
                break;
            case ConstantManager.SAVE_CHARACTER_EVENT:
                mCharacterPageCount++;
                Log.d(TAG, "inc pageCount => " + String.valueOf(mCharacterPageCount));
                break;
            case ConstantManager.NETWORK_ERROR_EVENT:
                if (mView != null) {
                    mView.showError(R.string.network_error_message);
                }
                break;
        }

        if (mHouseCount >= ConstantManager.HOUSE_COUNT & mCharacterPageCount >= ConstantManager.PAGE_COUNT) {
            if (mView != null) {
                Log.d(TAG, "all loaded, hide SplashView");
                mModel.setLoadState((byte) 2);
                mModel.setLoaded();
                mView.hideProgress();
                mView.close();
            }
        }
    }
}
