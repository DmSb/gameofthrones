package com.dmisb.gameofthrones.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dmisb.gameofthrones.GameOfThronesApp;

public class NetworkStatusChecker {

    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) GameOfThronesApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return  activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
