package com.dmisb.gameofthrones.utils;

public interface ConstantManager {

    String TAG_PREFIX = "GOT ";

    // region DataBase =============================================================================

    String FIELD_PRIMARY = "INTEGER PRIMARY KEY";
    String FIELD_INTEGER = "INTEGER";
    String FIELD_TEXT    = "TEXT";

    // endregion

    // region Network ==============================================================================

    String BASE_URL = "http://www.anapioficeandfire.com/api/";

    int MAX_CONNECT_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;

    // House Stark of Winterfell
    int HOUSE_STARK_ID = 362;
    // House Lannister of Casterly Rock
    int HOUSE_LANNISTER_ID = 229;
    // House Targaryen of King's Landing
    int HOUSE_TARGARYEN_ID = 378;

    int HOUSE_COUNT = 3;
    int PAGE_COUNT = 43;

    String DATA_LOADED_KEY = "DATA_LOADED_KEY";

    // endregion

    // region Events ===============================================================================

    String SAVE_HOUSE_EVENT = "SAVE_HOUSE_EVENT";
    String SAVE_CHARACTER_EVENT = "SAVE_CHARACTER_EVENT";
    String NETWORK_ERROR_EVENT = "NETWORK_ERROR_EVENT";

    // endregion

    // region SharedPreferences ====================================================================

    String ITEM_ID_KEY = "ITEM_ID_KEY";
    String ITEM_NAME_KEY = "ITEM_NAME_KEY ";
    String ITEM_BORN_KEY = "ITEM_BORN_KEY";
    String ITEM_TITLES_KEY = "ITEM_TITLES_KEY";
    String ITEM_DIED_KEY = "ITEM_DIED_KEY";
    String ITEM_ALIASES_KEY = "ITEM_ALIASES_KEY ";
    String ITEM_FATHER_KEY  = "ITEM_FATHER_KEY";
    String ITEM_MOTHER_KEY = "ITEM_MOTHER_KEY";
    String ITEM_WORD_KEY = "ITEM_WORD_KEY";
    String ITEM_HOUSE_KEY = "ITEM_HOUSE_KEY";

    // endregion
}
