package com.dmisb.gameofthrones.utils;

public class StringUtils {

    public static String shortName(String className) {
        return className.substring(className.lastIndexOf(".") + 1);
    }
}
