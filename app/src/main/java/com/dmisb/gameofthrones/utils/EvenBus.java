package com.dmisb.gameofthrones.utils;

public class EvenBus {

    public static class EventDb {

        private String mMessage;

        public EventDb(String message) {
            mMessage = message;
        }

        public String getMessage() {
            return mMessage;
        }
    }
}
