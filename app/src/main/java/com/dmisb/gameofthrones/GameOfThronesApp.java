package com.dmisb.gameofthrones;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import com.dmisb.gameofthrones.data.storage.DataBaseHelper;
import com.facebook.stetho.Stetho;
import com.squareup.otto.Bus;

public class GameOfThronesApp extends Application {

    private static Context sContext;
    private static SharedPreferences sSharedPreferences;
    private static SQLiteDatabase sDatabase;
    private static Bus sBus;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sDatabase = new DataBaseHelper(this).getWritableDatabase();
        sBus = new Bus();

        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    public static SQLiteDatabase getDatabase() {
        return sDatabase;
    }

    public static Bus getBus() {
        return sBus;
    }
}
