package com.dmisb.gameofthrones.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dmisb.gameofthrones.R;
import com.dmisb.gameofthrones.data.storage.Character;
import com.dmisb.gameofthrones.databinding.ActivityMainBinding;
import com.dmisb.gameofthrones.mvp.presenters.impl.MainPresenter;
import com.dmisb.gameofthrones.mvp.views.IMainView;
import com.dmisb.gameofthrones.ui.adapters.CharacterAdapter;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.StringUtils;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IMainView {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(MainActivity.class.getName());
    private ActivityMainBinding mBinding;
    private final MainPresenter mPresenter = MainPresenter.getInstance();
    private LinearLayoutManager mLayoutManager;


    // region Life Cycle ===========================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupToolBar();
        setupRecycledView();
        setupDrawer();

        mPresenter.takeView(this);
        mPresenter.initView();

        setPagerListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (mPresenter.getState() != null) {
            Log.d(TAG, "onResume, restore state");
            mLayoutManager.onRestoreInstanceState(mPresenter.getState());
        }
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause, save state");
        mPresenter.setState(mLayoutManager.onSaveInstanceState());
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    // endregion

    // region Setup Activity =======================================================================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Enter character name");

        if (!mPresenter.getQuery().isEmpty()) {
            searchItem.expandActionView();
            searchView.setQuery(mPresenter.getQuery(), true);
            searchView.clearFocus();
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mPresenter.onSearch(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mBinding.drawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setupToolBar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupRecycledView() {
        mLayoutManager = new LinearLayoutManager(this);
        mBinding.recycledView.setLayoutManager(mLayoutManager);
        mBinding.recycledView.setHasFixedSize(true);
    }

    private void setupDrawer() {
        NavigationView navigationView = mBinding.navigationView;
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    mBinding.drawerLayout.closeDrawer(GravityCompat.START);

                    int tabId = 0;
                    switch (item.getItemId()) {
                        case R.id.lannister_item:
                            tabId = 1;
                            break;
                        case R.id.targaryen_item:
                            tabId = 2;
                            break;
                    }

                    try {
                        mBinding.tabLayout.getTabAt(tabId).select();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    return true;
                }
            });
        }
    }

    private void setPagerListener() {
        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPresenter.onTabSelected(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    // endregion

    // region IMainView ============================================================================

    @Override
    public void showHouse(List<Character> characters, int houseId) {
        if (characters.size() > 0) {
            CharacterAdapter adapter = new CharacterAdapter(MainActivity.this,
                    characters,
                    houseId,
                    new CharacterAdapter.CharacterViewHolder.CustomClickListener() {
                        @Override
                        public void onItemClickListener(View v, int position) {
                            Intent intent = new Intent(MainActivity.this, CharacterActivity.class);
                            mPresenter.fillExtra(intent, position);
                            startActivity(intent);
                        }
                    }
            );
            mBinding.recycledView.swapAdapter(adapter, false);
        }
    }

    @Override
    public void setupPager(int tabId) {
        mBinding.tabLayout.addTab(mBinding.tabLayout.newTab().setText(R.string.starks));
        mBinding.tabLayout.addTab(mBinding.tabLayout.newTab().setText(R.string.lannisters));
        mBinding.tabLayout.addTab(mBinding.tabLayout.newTab().setText(R.string.targaryens));
        try {
            mBinding.tabLayout.getTabAt(tabId).select();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    // endregion
}
