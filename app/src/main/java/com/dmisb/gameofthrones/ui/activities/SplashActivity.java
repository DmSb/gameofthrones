package com.dmisb.gameofthrones.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.dmisb.gameofthrones.R;
import com.dmisb.gameofthrones.data.managers.DataManager;
import com.dmisb.gameofthrones.mvp.presenters.impl.SplashPresenter;
import com.dmisb.gameofthrones.mvp.views.ISplashView;

public class SplashActivity extends BaseActivity implements ISplashView {

    private DataManager mDataManager;
    private final SplashPresenter mPresenter = SplashPresenter.getInstance();

    // region Life Cycle ===========================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mDataManager = DataManager.getInstance();

        mPresenter.takeView(this);
        mPresenter.initView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDataManager.getBus().unregister(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    // endregion

    // region ISplashView ==========================================================================

    @Override
    public void close() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public void showError(int resId) {
        showToast(getString(resId));
    }

    // endregion
}
