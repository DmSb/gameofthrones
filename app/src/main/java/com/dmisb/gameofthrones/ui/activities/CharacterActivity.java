package com.dmisb.gameofthrones.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.View;

import com.dmisb.gameofthrones.R;
import com.dmisb.gameofthrones.databinding.ActivityCharacterBinding;
import com.dmisb.gameofthrones.mvp.presenters.impl.CharacterPresenter;
import com.dmisb.gameofthrones.mvp.views.ICharacterView;

public class CharacterActivity extends BaseActivity implements ICharacterView {

    private ActivityCharacterBinding mBinding;
    private final CharacterPresenter mPresenter = CharacterPresenter.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_character);

        setupToolBar();

        mPresenter.takeView(this);
        mPresenter.initView(getIntent());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    private void setupToolBar() {

        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
        }
    }

    // region ICharacterView =======================================================================

    @Override
    public void showMessage(String message) {
        showSnackBar(mBinding.coordinatorLayout, message);
    }

    @Override
    public void setImage(int resId) {
        mBinding.coat.setImageResource(resId);
    }

    @Override
    public void setName(String name) {
        mBinding.itemName.setText(name);
    }

    @Override
    public void setWords(String words) {
        mBinding.characterInfo.itemWords.setText(words);
    }

    @Override
    public void setBorn(String born) {
        mBinding.characterInfo.itemBorn.setText(born);
    }

    @Override
    public void setTitles(String titles) {
        mBinding.characterInfo.itemTitles.setText(titles);
    }

    @Override
    public void setAliases(String aliases) {
        mBinding.characterInfo.itemAliases.setText(aliases);
    }

    @Override
    public void setFather(String fatherName, int visibility) {
        mBinding.characterInfo.itemFather.setText(fatherName);
        mBinding.characterInfo.itemFather.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            mBinding.characterInfo.itemFather.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onParentClick(true);
                }
            });
        }
    }

    @Override
    public void setMother(String motherName, int visibility) {
        mBinding.characterInfo.itemMother.setText(motherName);
        mBinding.characterInfo.itemMother.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            mBinding.characterInfo.itemMother.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onParentClick(false);
                }
            });
        }
    }

    @Override
    public void setDied(String died) {
        if (!died.isEmpty()) {
            showMessage("Died : " + died);
        }
    }

    // endregion

    private void onParentClick(boolean isFather) {
        Intent intent = new Intent(CharacterActivity.this, CharacterActivity.class);
        mPresenter.fillExtra(intent, isFather);
        startActivity(intent);
        finish();
    }
}
