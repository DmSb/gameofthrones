package com.dmisb.gameofthrones.ui.adapters;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmisb.gameofthrones.R;
import com.dmisb.gameofthrones.data.storage.Character;
import com.dmisb.gameofthrones.databinding.CharacterItemBinding;
import com.dmisb.gameofthrones.utils.ConstantManager;

import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {

    private Context mContext;
    private List<Character> mList;
    private CharacterViewHolder.CustomClickListener mListener;
    private int mHouseId;
    private Drawable mStarkIco, mLannisterIco, mTargaryenIco;

    public CharacterAdapter(Context context,
                            List<Character> list,
                            int houseId,
                            CharacterViewHolder.CustomClickListener listener) {
        mContext = context;
        mList = list;
        mHouseId = houseId;
        mListener = listener;
        mStarkIco = mContext.getResources().getDrawable(R.drawable.ic_stark_icon);
        mLannisterIco = mContext.getResources().getDrawable(R.drawable.ic_lanister_icon);
        mTargaryenIco = mContext.getResources().getDrawable(R.drawable.ic_targarien_icon);
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CharacterViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.character_item, parent, false),
                mListener
        );
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        Character model = mList.get(position);
        switch (mHouseId) {
            case ConstantManager.HOUSE_STARK_ID:
                holder.mBinding.itemImg.setImageDrawable(mStarkIco);
                //Picasso.with(mContext)
                //        .load(R.drawable.stark_icon)
                //        .into(holder.mBinding.itemImg);
                break;
            case ConstantManager.HOUSE_LANNISTER_ID:
                holder.mBinding.itemImg.setImageDrawable(mLannisterIco);
                //Picasso.with(mContext)
                //        .load(R.drawable.lanister_icon)
                //        .into(holder.mBinding.itemImg);
                break;
            case ConstantManager.HOUSE_TARGARYEN_ID:
                holder.mBinding.itemImg.setImageDrawable(mTargaryenIco);
                //Picasso.with(mContext)
                //        .load(R.drawable.targarien_icon)
                //        .into(holder.mBinding.itemImg);
                break;
        }
        holder.mBinding.itemName.setText(model.name);
        holder.mBinding.itemTitles.setText(model.titles);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class CharacterViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        CharacterItemBinding mBinding;
        CustomClickListener mClickListener;

        CharacterViewHolder(View itemView, CustomClickListener clickListener) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);
            mClickListener = clickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                int position = getAdapterPosition();
                mClickListener.onItemClickListener(view, position);
            }
        }

        public interface CustomClickListener {
            void onItemClickListener(View v, int position);
        }
    }
}
