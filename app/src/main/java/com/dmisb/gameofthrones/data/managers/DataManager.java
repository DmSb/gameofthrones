package com.dmisb.gameofthrones.data.managers;

import android.text.TextUtils;
import android.util.Log;

import com.dmisb.gameofthrones.data.network.RestService;
import com.dmisb.gameofthrones.data.network.ServiceGenerator;
import com.dmisb.gameofthrones.data.network.res.CharacterRes;
import com.dmisb.gameofthrones.data.network.res.HouseRes;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.GameOfThronesApp;
import com.dmisb.gameofthrones.utils.StringUtils;
import com.squareup.otto.Bus;

import java.util.List;

import retrofit2.Call;

public class DataManager {

    private static final String TAG = ConstantManager.TAG_PREFIX +
            StringUtils.shortName(DataManager.class.getName());
    private static DataManager INSTANCE = null;

    private final PreferencesManager mPreferencesManager;
    private final RestService mRestService;
    private final Bus mBus;

    private DataManager() {
        Log.d(TAG, "constructor");
        mPreferencesManager = new PreferencesManager();
        mRestService = ServiceGenerator.createService(RestService.class);
        mBus = GameOfThronesApp.getBus();
    }

    public static DataManager getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public Bus getBus() {
        return mBus;
    }

    // region Network

    public Call<HouseRes> getHouse(int houseId) {
        return mRestService.getHouse(houseId);
    }

    public Call<List<CharacterRes>> getCharacters(int pageNum) {
        return mRestService.getCaracters(pageNum, 50);
    }

    // endregion
}
