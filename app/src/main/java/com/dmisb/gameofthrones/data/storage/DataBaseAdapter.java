package com.dmisb.gameofthrones.data.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.dmisb.gameofthrones.data.managers.DataManager;
import com.dmisb.gameofthrones.data.network.res.CharacterRes;
import com.dmisb.gameofthrones.data.network.res.HouseRes;
import com.dmisb.gameofthrones.data.storage.DataBaseSchema.CharactersTable.CharactersFields;
import com.dmisb.gameofthrones.data.storage.DataBaseSchema.HousesTable.HousesFields;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.utils.EvenBus;
import com.dmisb.gameofthrones.GameOfThronesApp;

import java.util.ArrayList;
import java.util.List;

public class DataBaseAdapter {

    public static HouseRes getHouse(int houseId) {

        HouseRes houseRes = null;
        SQLiteDatabase database = GameOfThronesApp.getDatabase();
        Cursor cursor = database.query(
                DataBaseSchema.HousesTable.NAME,
                null,
                HousesFields.id + "=" + String.valueOf(houseId),
                null, null, null, null
        );

        try {
            if (cursor.getCount() <= 0) {
                return null;
            }

            houseRes = new HouseRes();
            cursor.moveToFirst();
            houseRes.id = cursor.getInt(cursor.getColumnIndex(HousesFields.id));
            houseRes.name = cursor.getString(cursor.getColumnIndex(HousesFields.name));
            houseRes.region = cursor.getString(cursor.getColumnIndex(HousesFields.region));
            houseRes.coatOfArms = cursor.getString(cursor.getColumnIndex(HousesFields.coatOfArms));
            houseRes.words = cursor.getString(cursor.getColumnIndex(HousesFields.words));
            houseRes.currentLord = cursor.getString(cursor.getColumnIndex(HousesFields.currentLord));
            houseRes.heir = cursor.getString(cursor.getColumnIndex(HousesFields.heir));
            houseRes.overlord = cursor.getString(cursor.getColumnIndex(HousesFields.overlord));
            houseRes.founded = cursor.getString(cursor.getColumnIndex(HousesFields.founded));
            houseRes.founder = cursor.getString(cursor.getColumnIndex(HousesFields.founder));
            houseRes.diedOut = cursor.getString(cursor.getColumnIndex(HousesFields.diedOut));
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return houseRes;
    }

    public static boolean saveHouse(HouseRes houseRes) {

        boolean result = false;
        String s;

        SQLiteDatabase database = GameOfThronesApp.getDatabase();
        try {
            database.beginTransaction();

            ContentValues value = new ContentValues();
            value.put(HousesFields.id, houseRes.id);
            value.put(HousesFields.name, houseRes.name);
            value.put(HousesFields.region, houseRes.region);
            value.put(HousesFields.coatOfArms, houseRes.coatOfArms);
            value.put(HousesFields.words, houseRes.words);
            value.put(HousesFields.currentLord, convertCharacterUrl(houseRes.currentLord));
            value.put(HousesFields.heir, convertCharacterUrl(houseRes.heir));
            value.put(HousesFields.overlord, convertHouseUrl(houseRes.overlord));
            value.put(HousesFields.founded, houseRes.founded);
            value.put(HousesFields.founder, convertCharacterUrl(houseRes.founder));
            value.put(HousesFields.diedOut, houseRes.diedOut);
            database.insert(DataBaseSchema.HousesTable.NAME, null, value);

            database.setTransactionSuccessful();
            DataManager.getInstance().getBus().post(new EvenBus.EventDb(ConstantManager.SAVE_HOUSE_EVENT));
            result = true;
        } finally {
            database.endTransaction();
        }
        return result;
    }

    public static boolean saveCharacters(List<CharacterRes> characters) {

        boolean result = false;

        SQLiteDatabase database = GameOfThronesApp.getDatabase();
        try {
            database.beginTransaction();
            ContentValues value = new ContentValues();
            int id;
            String tmp, p;

            for (CharacterRes character: characters) {
                value.clear();

                tmp = ",";
                if (character.allegiances.size() > 0) {
                    for (String s: character.allegiances) {
                        p = convertHouseUrl(s);
                        id = Integer.valueOf(p);
                        if (id > 0  && (id == ConstantManager.HOUSE_LANNISTER_ID ||
                                id == ConstantManager.HOUSE_STARK_ID ||
                                id == ConstantManager.HOUSE_TARGARYEN_ID)) {
                            tmp += p + ",";
                        }
                    }
                }

                if (!tmp.equals(",")) {

                    value.put(CharactersFields.allegiances, tmp);

                    id = Integer.valueOf(convertCharacterUrl(character.url));
                    value.put(CharactersFields.id, id);
                    value.put(CharactersFields.name, character.name);
                    value.put(CharactersFields.gender, character.gender);
                    value.put(CharactersFields.culture, character.culture);
                    value.put(CharactersFields.born, character.born);

                    tmp = "";
                    if (character.tvSeries.size() > 0) {
                        if (character.died.isEmpty()) {
                            tmp = character.tvSeries.get(character.tvSeries.size() - 1);
                        } else {
                            tmp = character.died + ",  " + character.tvSeries.get(character.tvSeries.size() - 1);
                        }
                    }

                    value.put(CharactersFields.died, tmp);
                    value.put(CharactersFields.father, convertCharacterUrl(character.father));
                    value.put(CharactersFields.mother, convertCharacterUrl(character.mother));
                    value.put(CharactersFields.spouse, convertCharacterUrl(character.spouse));


                    if (character.titles.size() > 0) {
                        tmp = "";
                        for (String s : character.titles) {
                            if (tmp.isEmpty()) {
                                tmp = s;
                            } else {
                                tmp += ", " + s;
                            }
                        }

                        if (!tmp.isEmpty()) {
                            value.put(CharactersFields.titles, tmp);
                        }
                    }

                    if (character.aliases.size() > 0) {
                        tmp = "";
                        for (String s : character.aliases) {
                            if (tmp.isEmpty()) {
                                tmp = s;
                            } else {
                                tmp += ", " + s;
                            }
                        }
                        if (!tmp.isEmpty()) {
                            value.put(CharactersFields.aliases, tmp);
                        }
                    }

                    database.insert(DataBaseSchema.CharactersTable.NAME, null, value);
                }
            }

            database.setTransactionSuccessful();
            DataManager.getInstance().getBus().post(new EvenBus.EventDb(ConstantManager.SAVE_CHARACTER_EVENT));
            result = true;
        } finally {
            database.endTransaction();
        }

        return result;
    }

    public static List<Character> getCharacters(int houseId, String name) {

        List<Character> characterList = new ArrayList<>();
        SQLiteDatabase database = GameOfThronesApp.getDatabase();
        Cursor cursor;
        name = name.toUpperCase();

        if (name.isEmpty()) {
            cursor = database.query(
                    DataBaseSchema.CharactersTable.NAME,
                    null, null, null, null, null, null, null);
        } else {
            cursor = database.query(
                    DataBaseSchema.CharactersTable.NAME,
                    null,
                    "upper(" + CharactersFields.name + ") like '" + name + "%'",
                    null, null, null, null, null);
        }

        try {
            if (cursor.getCount() <= 0) {
                return characterList;
            }

            Character character;
            cursor.moveToFirst();
            do {
                if (cursor.getString(cursor.getColumnIndex(CharactersFields.allegiances)).indexOf(String.valueOf(houseId)) >= 0) {

                    character = new Character();
                    character.id = cursor.getInt(cursor.getColumnIndex(CharactersFields.id));
                    character.name = cursor.getString(cursor.getColumnIndex(CharactersFields.name));
                    character.born = cursor.getString(cursor.getColumnIndex(CharactersFields.born));
                    character.died = cursor.getString(cursor.getColumnIndex(CharactersFields.died));
                    character.aliases = cursor.getString(cursor.getColumnIndex(CharactersFields.aliases));
                    character.titles = cursor.getString(cursor.getColumnIndex(CharactersFields.titles));
                    character.father = cursor.getInt(cursor.getColumnIndex(CharactersFields.father));
                    character.mother = cursor.getInt(cursor.getColumnIndex(CharactersFields.mother));
                    characterList.add(character);
                }
            }
            while (cursor.moveToNext()) ;

        } finally {
            cursor.close();
        }
        return characterList;
    }

    public static Character getCharacter(int id) {

        SQLiteDatabase database = GameOfThronesApp.getDatabase();
        Cursor cursor = database.query(
                DataBaseSchema.CharactersTable.NAME,
                null,
                CharactersFields.id + " = " + String.valueOf(id),
                null,
                null, null, null, null
        );

        Character character = new Character();
        try {
            if (cursor.getCount() <= 0) {
                return null;
            }

            cursor.moveToFirst();
            character.id = cursor.getInt(cursor.getColumnIndex(CharactersFields.id));
            character.name = cursor.getString(cursor.getColumnIndex(CharactersFields.name));
            character.born = cursor.getString(cursor.getColumnIndex(CharactersFields.born));
            character.died = cursor.getString(cursor.getColumnIndex(CharactersFields.died));
            character.titles = cursor.getString(cursor.getColumnIndex(CharactersFields.titles));
            character.aliases = cursor.getString(cursor.getColumnIndex(CharactersFields.aliases));
            character.father = cursor.getInt(cursor.getColumnIndex(CharactersFields.father));
            character.mother = cursor.getInt(cursor.getColumnIndex(CharactersFields.mother));

        } finally {
            cursor.close();
        }
        return character;
    }

    public static void deleteData() {
        SQLiteDatabase database = GameOfThronesApp.getDatabase();
        try {
            database.beginTransaction();

            database.execSQL("delete from " + DataBaseSchema.HousesTable.NAME);
            database.execSQL("delete from " + DataBaseSchema.CharactersTable.NAME);

            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    private static String convertHouseUrl(String url) {

        String s = "";
        for (int i = url.length() -1; i >= 0; i--) {
            if (java.lang.Character.isDigit(url.charAt(i))) {
                s = url.charAt(i) + s;
            } else {
                break;
            }
        }

        if (s.trim().isEmpty()) {
            s = "0";
        }

        try {
            if (Integer.parseInt(s) > 0) {
                return s;
            } else {
                return "0";
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "0";
        }
    }

    private static String convertCharacterUrl(String url) {

        String s = "";
        for (int i = url.length() -1; i >= 0; i--) {
            if (java.lang.Character.isDigit(url.charAt(i))) {
                s = url.charAt(i) + s;
            } else {
                break;
            }
        }

        if (s.trim().isEmpty()) {
            s = "0";
        }

        try {
            if (Integer.parseInt(s) > 0) {
                return s;
            } else {
                return "0";
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "0";
        }
    }
}
