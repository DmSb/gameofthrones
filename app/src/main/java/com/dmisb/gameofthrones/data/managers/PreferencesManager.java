package com.dmisb.gameofthrones.data.managers;

import android.content.SharedPreferences;

import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.GameOfThronesApp;

public class PreferencesManager {

    private SharedPreferences mSharedPreferences;

    PreferencesManager() {
        mSharedPreferences = GameOfThronesApp.getSharedPreferences();
    }

    public void setDataLoaded() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(ConstantManager.DATA_LOADED_KEY, true);
        editor.apply();
    }

    public boolean getDataLoaded() {
        return mSharedPreferences.getBoolean(ConstantManager.DATA_LOADED_KEY, false);
    }
}
