package com.dmisb.gameofthrones.data.network;

import com.dmisb.gameofthrones.data.network.res.CharacterRes;
import com.dmisb.gameofthrones.data.network.res.HouseRes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestService {

    @GET("houses/{house_id}")
    Call<HouseRes> getHouse (@Path("house_id") int houseId);

    @GET("characters")
    Call<List<CharacterRes>> getCaracters(
            @Query("page") int page,
            @Query("pageSize") int pageSize
    );
}
