package com.dmisb.gameofthrones.data.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dmisb.gameofthrones.data.storage.DataBaseSchema.CharactersTable;
import com.dmisb.gameofthrones.data.storage.DataBaseSchema.CharactersTable.CharactersFields;
import com.dmisb.gameofthrones.data.storage.DataBaseSchema.HousesTable;
import com.dmisb.gameofthrones.data.storage.DataBaseSchema.HousesTable.HousesFields;
import com.dmisb.gameofthrones.utils.ConstantManager;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "gameOfThrones.db";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.beginTransaction();

            sqLiteDatabase.execSQL("create table " + HousesTable.NAME + " (" +
                    HousesFields.id + " " + ConstantManager.FIELD_PRIMARY + ", " +
                    HousesFields.name + " " + ConstantManager.FIELD_TEXT + ", " +
                    HousesFields.region + " " + ConstantManager.FIELD_TEXT + ", " +
                    HousesFields.coatOfArms + " " + ConstantManager.FIELD_TEXT + ", " +
                    HousesFields.words + " " + ConstantManager.FIELD_TEXT + ", " +
                    HousesFields.currentLord + " " + ConstantManager.FIELD_INTEGER + ", " +
                    HousesFields.heir + " " + ConstantManager.FIELD_INTEGER + ", " +
                    HousesFields.overlord + " " + ConstantManager.FIELD_INTEGER + ", " +
                    HousesFields.founded + " " + ConstantManager.FIELD_TEXT + ", " +
                    HousesFields.founder + " " + ConstantManager.FIELD_INTEGER + ", " +
                    HousesFields.diedOut + " " + ConstantManager.FIELD_TEXT + ")"
            );

            sqLiteDatabase.execSQL("create table " + CharactersTable.NAME + " (" +
                    CharactersFields.id + " " + ConstantManager.FIELD_PRIMARY + ", " +
                    CharactersFields.name + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.gender + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.culture + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.born + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.died + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.titles + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.aliases + " " + ConstantManager.FIELD_TEXT + ", " +
                    CharactersFields.father + " " + ConstantManager.FIELD_INTEGER+ ", " +
                    CharactersFields.mother + " " + ConstantManager.FIELD_INTEGER + ", " +
                    CharactersFields.spouse + " " + ConstantManager.FIELD_INTEGER + ", " +
                    CharactersFields.allegiances + " " + ConstantManager.FIELD_TEXT + ")"
            );

            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
