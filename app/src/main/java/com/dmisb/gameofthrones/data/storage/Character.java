package com.dmisb.gameofthrones.data.storage;

public class Character {

    public int id;
    public String name;
    public String born;
    public String died;
    public int father;
    public int mother;
    public String titles;
    public String aliases;
}
