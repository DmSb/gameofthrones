package com.dmisb.gameofthrones.data.network;

import com.dmisb.gameofthrones.data.network.interceptors.HeaderInterceptor;
import com.dmisb.gameofthrones.utils.ConstantManager;
import com.dmisb.gameofthrones.GameOfThronesApp;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static OkHttpClient.Builder sHttpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder sBuilder =
            new Retrofit.Builder()
                    .baseUrl(ConstantManager.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        sHttpClient.addInterceptor(new HeaderInterceptor());
        sHttpClient.addInterceptor(logging);
        sHttpClient.connectTimeout(ConstantManager.MAX_CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        sHttpClient.readTimeout(ConstantManager.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS);
        sHttpClient.cache(new Cache(GameOfThronesApp.getContext().getCacheDir(), Integer.MAX_VALUE));
        sHttpClient.addNetworkInterceptor(new StethoInterceptor());

        Retrofit retrofit = sBuilder
                .client(sHttpClient.build())
                .build();
        return  retrofit.create(serviceClass);
    }
}
