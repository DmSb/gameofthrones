package com.dmisb.gameofthrones.data.storage;

class DataBaseSchema {

    static final class HousesTable {

        static final String NAME = "HOUSES";

        static final class HousesFields{

            static final String id = "ID";
            static final String name = "NAME";
            static final String region = "REGION";
            static final String coatOfArms = "COAT_OF_ARMS";
            static final String words = "WORDS";
            static final String currentLord = "CURRENT_LORD";
            static final String heir = "HEIR";
            static final String overlord = "OVERLORD";
            static final String founded = "FOUNDED";
            static final String founder = "FOUNDER";
            static final String diedOut = "DIED_OUT";
        }
    }

    static final class CharactersTable {

        static final String NAME = "CHARACTERS";

        static final class CharactersFields {
            static final String id = "ID";
            static final String name = "NAME";
            static final String gender = "GENDER";
            static final String culture = "CULTURE";
            static final String born = "BORN";
            static final String died = "DIED";
            static final String titles = "TITLES";
            static final String aliases = "ALIASES";
            static final String father = "FATHER";
            static final String mother = "MOTHER";
            static final String spouse = "SPOUSE";
            static final String allegiances = "ALLEGIANCE";
        }
    }
}
